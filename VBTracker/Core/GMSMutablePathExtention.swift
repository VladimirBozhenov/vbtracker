//
//  GMSMutablePathExtention.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 20/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import GoogleMaps

extension GMSMutablePath {
    
    func getCorrdinatesFromPath() -> [Coordinate] {
        var coordinates = [Coordinate]()
        for point in 0..<self.count() {
            let coordinate = Coordinate()
            coordinate.latitude = self.coordinate(at: point).latitude
            coordinate.longitude = self.coordinate(at: point).longitude
            coordinates.append(coordinate)
        }
        return coordinates
    }
}
