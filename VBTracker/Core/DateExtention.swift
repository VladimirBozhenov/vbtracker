//
//  DateExtention.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 20/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

extension Date {
    func stringFromDate() -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "HH:mm:ss"
        let date = dateFormater.string(from: self)
        return date
    }
}
