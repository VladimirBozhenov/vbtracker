//
//  ViewControllerHelper.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 24/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

protocol StoryboardIdentifier {
    static var storyboardIdentifier: String { get }
}

extension UIViewController: StoryboardIdentifier {}

extension StoryboardIdentifier where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}

extension UIStoryboard {
    func instantiateVC<T: UIViewController>(type: T.Type) -> T {
        guard let viewController = self.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("View contoller with name \(T.storyboardIdentifier) doesn't exist")
        }
        return viewController
    }
}
