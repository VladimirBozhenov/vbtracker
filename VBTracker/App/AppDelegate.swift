//
//  AppDelegate.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 12/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import GoogleMaps
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var blurEffectView: UIVisualEffectView?
    let center = UNUserNotificationCenter.current()
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let googleAPIKey = Environment.googleAPIKey
        GMSServices.provideAPIKey(googleAPIKey)
        
        var controller: UIViewController
        if UserDefaults.standard.bool(forKey: "isAutorized") {
            controller = UIStoryboard(name: "Map",
                                      bundle: nil).instantiateVC(type: MapViewController.self)
        } else {
            controller = UIStoryboard(name: "Auth",
                                      bundle: nil).instantiateVC(type: AuthViewController.self)
        }
        window = UIWindow()
        window?.rootViewController = UINavigationController(rootViewController: controller)
        window?.makeKeyAndVisible()
        return true
    }
    
    private func makeNotificationContent() -> UNNotificationContent {
        let userActions = "User Actions"
        let content = UNMutableNotificationContent()
        content.title = "Вас давно не было."
        content.subtitle = "Уже 10 секунд прошло."
        content.body = "Возвращайтесь."
        content.sound = .default
        content.badge = 1
        content.categoryIdentifier = userActions
        let cancelAction = UNNotificationAction(identifier: "Cancel",
                                                title: "Cancel",
                                                options: [.destructive])
        let category = UNNotificationCategory(identifier: userActions,
                                              actions: [cancelAction],
                                              intentIdentifiers: [],
                                              options: [])
        center.setNotificationCategories([category])
        return content
    }
    
    func makeIntervalNotificatioTrigger() -> UNNotificationTrigger {
        return UNTimeIntervalNotificationTrigger(timeInterval: 20,
                                                 repeats: false)
    }
    
    func sendNotificatioRequest(content: UNNotificationContent,
                                trigger: UNNotificationTrigger) {
        let request = UNNotificationRequest(identifier: "alaram",
                                            content: content,
                                            trigger: trigger
        )
        
        let center = UNUserNotificationCenter.current()
        center.add(request) { error in
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        let blurEffect = UIBlurEffect(style: .light)
        self.blurEffectView = UIVisualEffectView(effect: blurEffect)
        self.blurEffectView?.frame = UIScreen.main.bounds
        self.blurEffectView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.window?.addSubview(blurEffectView!)
        center.requestAuthorization(options: [.alert,
                                              .sound,
                                              .badge]) { granted, error in
                                                if granted {
                                                    print("success")
                                                } else {
                                                    print("fail")
                                                }
        }
        center.getNotificationSettings { settings in
            switch settings.authorizationStatus {
            case .authorized:
                self.sendNotificatioRequest(content: self.makeNotificationContent(),
                                            trigger: self.makeIntervalNotificatioTrigger())
            default: print("error")
            }
        }
        center.delegate = self
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        self.blurEffectView?.removeFromSuperview()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("Default")
        case "Cancel":
            print("Cancel")
            UIApplication.shared.applicationIconBadgeNumber = 0
        default:
            print("Unknown action")
        }
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print(notification.request.identifier)
//        completionHandler([.alert,
//                           .sound,
//                           .badge])
    }
}

