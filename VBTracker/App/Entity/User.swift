//
//  User.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 23/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    @objc dynamic var login: String = ""
    @objc dynamic var password: String = ""
    
    convenience init(login: String,
         password: String) {
        self.init()
        self.login = login
        self.password = password
    }
    
    override class func primaryKey() -> String? {
        return "login"
    }
}
