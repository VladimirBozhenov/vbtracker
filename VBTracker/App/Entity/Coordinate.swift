//
//  Coordinate.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 20/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import RealmSwift

class Coordinate: Object {
    @objc dynamic var longitude: Double = 0
    @objc dynamic var latitude: Double = 0
}
