//
//  MapViewModel.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 17/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import GoogleMaps
import RealmSwift
import RxSwift
import RxCocoa

class MapViewModel {
    
    private let locationService = LocationService.instance
    private let realmService: RealmService = RealmServiceImpl()
    
    let route = GMSPolyline()
    private var routePath = GMSMutablePath()
    var isLocating = false
    var currentLocation = BehaviorRelay<CLLocation?>(value: nil)
    
    private let disposeBag = DisposeBag()
    
    func startLocating() {
        if !isLocating {
            routePath.removeAllCoordinates()
        }
        locationService.startLocation()
        isLocating = true
    }
    
    func stopLocating() {
        locationService.stopLocation()
        isLocating = false
    }
    
    func generateRoute(by location: CLLocation) {
        route.strokeColor = UIColor.red
        route.strokeWidth = 5
        routePath.add(location.coordinate)
        route.path = routePath
    }
    
    func saveRoute(completion: @escaping () -> ()) {
        let coordinates = self.routePath.getCorrdinatesFromPath()
        self.realmService.delete(coordinates)
        self.realmService.save(coordinates,
                               update: false)
        completion()
    }
    
    func updateCamera() -> GMSCameraUpdate {
        getRoute()
        let cameraUpdate = GMSCameraUpdate.fit(GMSCoordinateBounds(path: self.routePath))
        return cameraUpdate
    }
    
    private func getRoute() {
        guard let coordinates: [Coordinate] = realmService.load(Coordinate.self) else { return }
        let routePath = GMSMutablePath()
        for coordinate in coordinates {
            routePath.add(CLLocationCoordinate2D(latitude: coordinate.latitude,
                                                 longitude: coordinate.longitude))
        }
        self.route.path = routePath
        self.routePath = routePath
    }
    
    func observeLocation() {
        locationService
            .location
            .asObservable()
            .bind { [weak self] myLocation in
                guard let myLocation = myLocation,
                    let self = self else { return }
                self.generateRoute(by: myLocation)
                self.currentLocation.accept(myLocation)
        }.disposed(by: disposeBag)
    }
}
