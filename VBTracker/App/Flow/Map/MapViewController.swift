//
//  MapViewController.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 12/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import GoogleMaps
import RealmSwift
import RxSwift
import UIKit

class MapViewController: UIViewController {
    
    private let viewModel = MapViewModel()
    private let locationService = LocationService.instance
    private let disposeBag = DisposeBag()
    private var marker: GMSMarker?
    var onTakePicture: ((UIImage) -> Void)?
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var overviewButton: UIButton!
    @IBOutlet weak var selfieButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.configureMapAppearance()
        viewModel.observeLocation()
        moveMapToMyLocation()
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Route Tracker"
        startButton.setButtonStyle(with: .green)
        stopButton.setButtonStyle(with: .red)
        overviewButton.setButtonStyle(with: .yellow)
        selfieButton.setButtonStyle(with: .blue)
    }
    
    @IBAction func startButtonTapped(_ sender: Any) {
        mapView.configureMapAppearance()
        viewModel.startLocating()
        
    }
    
    @IBAction func stopButtonTapped(_ sender: Any) {
        viewModel.saveRoute {
            self.viewModel.stopLocating()
        }
    }
    
    @IBAction func overviewButtonTapped(_ sender: Any) {
        
        if viewModel.isLocating  {
            let alert = UIAlertController(title: "Предупреждение",
                                          message: "Включен треккер. Для продолжения необходимо его отключить",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Отключить",
                                          style: .default,
                                          handler: { [weak self] action in
                                            guard let self = self else { return }
                                            self.viewModel.saveRoute() {
                                                self.viewModel.stopLocating()
                                                self.viewModel.route.map = self.mapView
                                                let cameraUpdate = self.viewModel.updateCamera()
                                                self.mapView.animate(with: cameraUpdate)
                                            }
            }))
            alert.addAction(UIAlertAction(title: "Отмена",
                                          style: .cancel,
                                          handler: nil))
            present(alert,
                    animated: true)
        } else {
            
            let cameraUpdate = viewModel.updateCamera()
            viewModel.route.map = mapView
            mapView.animate(with: cameraUpdate)
        }
    }
    
    @IBAction func selfieButtonTapped(_ sender: UIButton) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else { return }
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .camera
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        present(imagePickerController, animated: true)
        
    }
    
    func moveMapToMyLocation() {
        viewModel
            .currentLocation
            .asObservable()
            .bind { [weak self] myLocation in
                guard let myLocation = myLocation,
                    let self = self else { return }
                self.marker?.map = nil
                self.marker = GMSMarker(position: myLocation.coordinate)
                self.mapView.animate(toLocation: myLocation.coordinate)
                let image = ImageStore().getImage()
                self.marker?.iconView = self.configureMarkerIconView(with: image)
                self.marker?.map = self.mapView
                self.viewModel.route.map = self.mapView
        }.disposed(by: disposeBag)
    }
    
    private func showSelfieModule(image: UIImage) {
        let controller = UIStoryboard(name: "Selfie", bundle: nil)
            .instantiateVC(type: SelfieViewController.self)
        
        controller.image = image
        navigationController?.pushViewController(controller,
                                                 animated: true)
    }
    
    private func configureMarkerIconView(with image: UIImage?) -> UIImageView {
        let markerView = UIImageView(frame: CGRect(x: 0,
                                                   y: 0,
                                                   width: 40,
                                                   height: 40))
        markerView.image = image
        markerView.tintColor = .white
        markerView.layer.cornerRadius = 20
        markerView.layer.borderColor = UIColor.white.cgColor
        markerView.layer.borderWidth = 1
        markerView.clipsToBounds = true
        return markerView
    }
}


extension MapViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) { [weak self] in
            guard let image = self?.extractImage(from: info) else { return }
            self?.onTakePicture?(image)
            ImageStore().save(image: image)
            self?.showSelfieModule(image: image)
        }
    }
    
    private func extractImage(from info: [UIImagePickerController.InfoKey : Any]) -> UIImage? {
        if let image = info[.editedImage] as? UIImage {
            return image
        } else if let image = info[.originalImage] as? UIImage {
            return image
        } else {
            return nil
        }
    }
}
