//
//  AuthInteractor.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 25/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

protocol AuthInteractor {
    
    var user: User { get set }
    
    func getUserWith(_ login: String) -> User?
    func saveUser()
    func updateUser()
    func markUserAuthorized()
}

class AuthInteractorImpl: AuthInteractor {
    
    private let realmService: RealmService
    var user = User()
    
    init(realmService: RealmService) {
        self.realmService = realmService
    }
    
    func getUserWith(_ login: String) -> User? {
        if let users = realmService.load(User.self) {
            return users.filter({ $0.login == login }).first
        }
        return nil
    }
    
    func saveUser() {
        realmService.save([user],
                          update: false)
    }
    
    func updateUser() {
        realmService.save([user],
                          update: true)
    }
    
    func markUserAuthorized() {
        UserDefaults.standard.set(true,
                                  forKey: "isAutorized")
    }
}
