//
//  AuthPresenter.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 24/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

protocol AuthPresenter {
    func loginOrBackButtonTapped()
    func registrationButtonTapped()
    func configureViewController()
}

class AuthPresenterImpl: AuthPresenter {
    var coordinator: AuthCoordinator
    var viewController: AuthViewController
    var interactor: AuthInteractor
    
    init(coordinator: AuthCoordinator,
         viewController: AuthViewController,
         interactor: AuthInteractor) {
        self.coordinator = coordinator
        self.viewController = viewController
        self.interactor = interactor
    }
    
    private func isFieldsEmpty() -> Bool {
        return viewController.loginTextField.text == ""
            || viewController.passwordTextField.text == ""
    }
    
    private func setViewController(to state: AuthViewControllerState) {
        viewController.viewControllerState = state
        configureViewController()
    }
    
    func loginOrBackButtonTapped() {
        switch viewController.viewControllerState {
        case .login:
            guard !isFieldsEmpty() else { return }
            let login = viewController.loginTextField.text
            let password = viewController.passwordTextField.text
            if let user = interactor.getUserWith(login!),
                user.password == password  {
                interactor.markUserAuthorized()
                coordinator.toMap()
            } else {
                setViewController(to: .registration)
            }
        case .registration:
            setViewController(to: .login)
        case .none:
            return
        }
    }
    
    func registrationButtonTapped() {
        switch viewController.viewControllerState {
        case .login:
            setViewController(to: .registration)
        case .registration:
            interactor.markUserAuthorized()
            if !isFieldsEmpty() {
                let login = viewController.loginTextField.text!
                let password = viewController.passwordTextField.text!
                interactor.user = User(login: login,
                                       password: password)
                if interactor.getUserWith(login) != nil {
                    interactor.updateUser()
                } else {
                    interactor.saveUser()
                }
                setViewController(to: .login)
                coordinator.toMap()
            }
        case .none:
            return
        }
    }
    
    func configureViewController() {
        viewController.navigationController?.navigationBar.prefersLargeTitles = true
        viewController.loginTextField.text = ""
        viewController.passwordTextField.text = ""
        viewController.registrationButton.setButtonStyle(with: .orange)
        viewController.loginOrBackButton.setButtonStyle(with: .orange)
        
        switch viewController.viewControllerState {
        case .login:
            viewController.loginOrBackButton.setTitle("Log In",
                                                      for: .normal)
            viewController.title = "Log In"
        case .registration:
            viewController.loginOrBackButton.setTitle("Back",
                                                      for: .normal)
            viewController.title = "Registration"
        case .none:
            return
        }
    }
}
