//
//  AuthViewController.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 23/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AuthViewController: UIViewController {

    var viewControllerState: AuthViewControllerState!
    var configurator: AuthConfigurator!
    var presenter: AuthPresenter!
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginOrBackButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    
    override func viewDidLoad() {
        viewControllerState = .login
        configurator = AuthConfiguratorImpl()
        configurator.configure(viewController: self)
        presenter.configureViewController()
        configureObservers()
    }
    
    @IBAction func loginOrBackButtonTapped(_ sender: UIButton) {
        presenter.loginOrBackButtonTapped()
        configureObservers()
    }
    
    @IBAction func registrationButtonTapped(_sender: UIButton) {
        presenter.registrationButtonTapped()
        configureObservers()
    }
    
    private func configureObservers() {
        let observedButton: UIButton = viewControllerState == .login ? loginOrBackButton : registrationButton
        let secondButton: UIButton = viewControllerState == .login ? registrationButton : loginOrBackButton
    Observable
        .combineLatest(loginTextField.rx.text,
                       passwordTextField.rx.text)
        .map { login, password in
            return !(login ?? "").isEmpty && (password ?? "").count >= 4
        }
        .subscribe(onNext: {[weak observedButton] ready in
            observedButton?.isEnabled = ready
            observedButton?.setButtonStyle(with: ready ? .orange : .gray)
            secondButton.isEnabled = true
            secondButton.setButtonStyle(with: .orange)
        }).disposed(by: disposeBag)
    }
}
