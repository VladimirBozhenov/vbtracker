//
//  AuthCoordinator.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 24/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

protocol AuthCoordinator {
    func toMap()
}

class AuthCoordinatorImpl: AuthCoordinator {
    var viewController: UIViewController
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
    
    func toMap() {
        let controller = UIStoryboard(name: "Map",
                                      bundle: nil).instantiateVC(type: MapViewController.self)
        viewController.navigationController?.pushViewController(controller,
                                                                animated: true)
    }
}
