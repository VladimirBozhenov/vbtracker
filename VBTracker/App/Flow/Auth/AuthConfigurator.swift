//
//  AuthConfigurator.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 24/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

protocol AuthConfigurator {
    func configure(viewController: AuthViewController)
}
class AuthConfiguratorImpl: AuthConfigurator {
    func configure(viewController: AuthViewController) {
        let coordinator = AuthCoordinatorImpl(viewController: viewController)
        let realmService = RealmServiceImpl()
        let interactor = AuthInteractorImpl(realmService: realmService)
        viewController.presenter = AuthPresenterImpl(coordinator: coordinator,
                                                     viewController: viewController,
                                                     interactor: interactor)
    }
}
