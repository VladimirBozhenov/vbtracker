//
//  AuthViewControllerState.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 24/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

enum AuthViewControllerState {
    case login
    case registration
}
