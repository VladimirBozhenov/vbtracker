//
//  ImageStore.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 14.10.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class ImageStore {
    
    private var selfie = "selfie.png"
    
    public func save(image: UIImage) {
        guard let data = image.pngData(),
            let directory = try? FileManager.default.url(for: .documentDirectory,
                                                         in: .userDomainMask,
                                                         appropriateFor: nil,
                                                         create: false) as NSURL else { return }
        do {
            try data.write(to: directory.appendingPathComponent(selfie)!,
                           options: .atomic)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    
    public func getImage() -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory,
                                                  in: .userDomainMask,
                                                  appropriateFor: nil,
                                                  create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(selfie).path)
        }
        return nil
    }
    
}

