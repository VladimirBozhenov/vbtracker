//
//  RealmService.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 20/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmService {
    
    func save <T: Object>(_ data: [T],
                          update: Bool)
    func load <T: Object>(_ type: T.Type) -> [T]?
    func delete <T: Object>(_ data: [T])
}

class RealmServiceImpl: RealmService {
    
    private let config: Realm.Configuration = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
    
    func save <T: Object>(_ data: [T],
                          update: Bool) {
        do {
            let realm = try Realm(configuration: config)
            try realm.write {
                update ? realm.add(data, update: .modified) : realm.add(data)
                let folderPath = realm.configuration.fileURL!
                print(folderPath)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func load <T: Object>(_ type: T.Type) -> [T]? {
        do {
            let realm = try Realm()
            let result = realm.objects(T.self)
            return result.map { $0 }
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func delete <T: Object>(_ data: [T]) {
        do {
            let realm = try Realm()
            let oldData = realm.objects(T.self)
            try realm.write {
                realm.delete(oldData)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
