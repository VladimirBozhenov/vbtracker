//
//  LocationService.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 20/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import GoogleMaps
import RxSwift
import RxCocoa

class LocationService: NSObject {
    
    let locationManager = CLLocationManager()
    let location = BehaviorRelay<CLLocation?>(value: nil)
    static let instance = LocationService()
    
    private override init() {
        super.init()
        configureLocationManager()
    }
    
    func configureLocationManager() {
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.requestAlwaysAuthorization()
    }
    
    func startLocation() {
        locationManager.startUpdatingLocation()
    }
    
    func stopLocation() {
        locationManager.stopUpdatingLocation()
    }
}

extension LocationService: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        self.location.accept(locations.last)
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}
