//
//  Environment.swift
//  VBTracker
//
//  Created by Vladimir Bozhenov on 27/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//
import Foundation

public enum Environment {
    enum Keys {
        enum Plist {
            static let googleAPIKey: String = "Google API key"
        }
    }
    private static let infoDictionary: [String: Any] = {
        guard let dict = Bundle.main.infoDictionary else {
            fatalError()
        }
        return dict
    }()
    
    static let googleAPIKey: String = {
        guard let key = Environment.infoDictionary[Keys.Plist.googleAPIKey] as? String else {
            fatalError()
        }
        return key
    }()
}
